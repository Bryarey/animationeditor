﻿using UnityEngine;
using System.Collections.Generic;

namespace AnimationEditor
{
    /// <summary>
    /// Represents single frame stored data (points positions)
    /// As it mentioned in the task, only points positions are stored in every frame. 
    /// </summary>
    public struct Frame
    {
        [SerializeField]
        private List<Vector2> m_positions;

        public List<Vector2> Positions
        {
            get
            {
                return m_positions;
            }

            set
            {
                m_positions = value;
            }
        }

        public Frame (List<Point> points)
        {
            m_positions = new List<Vector2>();
            for (int i = 0; i < points.Count; i++)
            {
                m_positions.Add (new Vector2(points[i].transform.position.x, points[i].transform.position.y));
            }
        }
    }
}
