﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace AnimationEditor
{
    /// <summary>
    /// Represents animated point. Implements interfaces IBeginDragHandler, IDragHandler, IEndDragHandler and IPointerClickHandler. So drag and click processed inside. 
    /// </summary>
	public class Point : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        //attached segments
        private List<Segment> m_segments = new List<Segment>();

        //is currently dragged
        private bool m_isDrag = false;

        public List<Segment> Segments
        {
            get
            {
                return m_segments;
            }

            set
            {
                m_segments = value;
            }
        }

        /// <summary>
        /// Static creator.
        /// </summary>
        /// <param name="position">Position for creation.</param>
        /// <returns>Created Point instance.</returns>
        public static Point Create(Vector3 position)
        {
            GameObject prefab = Resources.Load("Prefabs/Point") as GameObject;
            GameObject newPoint = Instantiate(prefab, position, Quaternion.identity) as GameObject;
            newPoint.transform.SetParent(Desk.Find().transform);
            Point pointBehaviour = newPoint.GetComponent<Point>();
            return pointBehaviour;
        }

        #region MonoBehaviourMessages
        private void Start()
        {
            Desk.Find().OnNewFileLoaded += HandleOnNewFileLoaded;
        }

        private void OnDestroy()
        {
            Desk.Find().OnNewFileLoaded -= HandleOnNewFileLoaded;
        }
        #endregion
        
        #region RecieveInput

        public void OnBeginDrag(PointerEventData eventData)
		{
            if (Desk.Find().Interactive)
            {
                m_isDrag = true;
            }
        }
		
		public void OnDrag(PointerEventData eventData)
		{
            if (m_isDrag)
            {
                transform.position = Desk.GetWorldLayerPosition(eventData.position, Layer.PointLayer);
                UpdateSegments();
            }
        }
		
		public void OnEndDrag(PointerEventData eventData)
		{
            if (m_isDrag)
            {
                m_isDrag = false;
                Desk.Find().SavePointPosition(this);
            }
        }

		public void OnPointerClick(PointerEventData eventData)
		{
            if (Desk.Find().Interactive)
            {
                if (m_isDrag == false)
                {
                    if (eventData.button == PointerEventData.InputButton.Left)
                    {
                        LeftButtonClick();
                    }
                    if (eventData.button == PointerEventData.InputButton.Right)
                    {
                        RightButtonClick();
                    }
                }
            }
        }
        
        #endregion

        #region ProcessInput

        /// <summary>
        /// Start or end of new segment`s preview mode
        /// </summary>
        private void LeftButtonClick()
        {
            if (Desk.Find().PreviewPoint == null)
            {
                Desk.Find().StartPreview(this);
            } else
            {
                Desk.Find().StopPreview(this);
            }
        }

        /// <summary>
        /// Remove point
        /// </summary>
        private void RightButtonClick()
        {
            Desk.Find().RemovePoint(this);
        }

        #endregion

        private void HandleOnNewFileLoaded()
        {
            Destroy(gameObject);
        }

        #region Utility

        /// <summary>
        /// Update positions of all point`s Segments
        /// </summary>
        private void UpdateSegments()
        {
            for (int i = 0; i < Segments.Count; i++)
            {
                Segments[i].UpdatePosition();
            }
        }

        /// <summary>
        /// Is two points connected by segment?
        /// </summary>
        /// <param name="firstPoint">first point</param>
        /// <param name="secondPoint">second point</param>
        /// <returns>returns segment connecting two points. If points are not connected, returns null.</returns>
        static public Segment IsPointsConnected (Point firstPoint, Point secondPoint)
        {
            Segment pointSegment = IsPointInSegments(firstPoint, secondPoint.Segments); //  Is firstPoint contained in one of secondPoint`s segments
            Segment targetSegment = IsPointInSegments(secondPoint, firstPoint.Segments);//  ...or vice versa?

            if (pointSegment != null )
            {
                return pointSegment;
            } else
            {
                if (targetSegment != null)
                {
                    return targetSegment;
                }
            }
            return null;
        }

        /// <summary>
        /// Is given point contained by any of segments in given array
        /// </summary>
        /// <param name="targetPoint">point to search</param>
        /// <param name="segments">array of segments</param>
        /// <returns>Returns segment, containing target point. If point is not contained by any segment in array, returns null.</returns>
        static private Segment IsPointInSegments(Point targetPoint, List<Segment> segments)
        {
            if (segments != null && segments.Count > 0)
            {
                for (int i = 0; i < segments.Count; i++)
                {
                    if (segments[i].StartPoint == targetPoint || segments[i].EndPoint == targetPoint)
                    {
                        return segments[i];
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
