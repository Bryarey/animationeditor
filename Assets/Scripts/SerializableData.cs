﻿using UnityEngine;
using System.Collections.Generic;


namespace AnimationEditor
{
    /// <summary>
    /// Performs data conversion to/from string format.
    /// </summary>
    public class SerializableData
    {
        #region Constants
        // Do not use "." as divider - floats are here!
        private const string SEGMENTS_HEADER = "Segments: \n";
        private const string FRAMES_HEADER = "\n\nFrames: ";
        private const string SEGMENTS_SEPARATOR = ", ";
        private const string SEGMENTS_START_END_SEPARATOR = "-";
        private const string POINTS_SEPARATOR = ", ";
        private const string FRAME_START = "\n[";
        private const string FRAME_END = "]";
        private const string XY_SEPARATOR = ":";
        private const string END_OF_BLOCK = ";";

        #endregion

        #region PrivateMembers

        private Segment[] m_segments;

        private Frame[] m_frames;

        #endregion

        #region PublicProperties

        public Segment[] Segments
        {
            get
            {
                return m_segments;
            }

            set
            {
                m_segments = value;
            }
        }

        public Frame[] Frames
        {
            get
            {
                return m_frames;
            }

            set
            {
                m_frames = value;
            }
        }

        #endregion

        #region Serialization

        /// <summary>
        /// Serialize data
        /// </summary>
        /// <param name="data">Data for serialization.</param>
        /// <returns>Serialized data as string.</returns>
        static public string Serialize(SerializableData data)
        {
            string result = "";

            result = result + SerializeSegments(data);

            result = result + SerializeFrames(data);

            return result;
        }

        /// <summary>
        /// Serialize segments.
        /// </summary>
        /// <param name="data">Data for serialization.</param>
        /// <returns>Serialized segments as string.</returns>
        static private string SerializeSegments(SerializableData data)
        {
            string segmentsAsString = SEGMENTS_HEADER;

            Desk desk = Desk.Find();

            string firstPoint;
            string secondPoint;
            for (int i = 0; i < data.Segments.Length; i++)
            {
                firstPoint = desk.Points.IndexOf(data.Segments[i].StartPoint).ToString();
                secondPoint = desk.Points.IndexOf(data.Segments[i].EndPoint).ToString();

                //Check for duplicates (segments are registerein in both start and end points):
                if (
                    segmentsAsString.Contains(SEGMENTS_SEPARATOR + firstPoint + SEGMENTS_START_END_SEPARATOR + secondPoint + SEGMENTS_SEPARATOR) == false &&
                    segmentsAsString.Contains(SEGMENTS_HEADER + firstPoint + SEGMENTS_START_END_SEPARATOR + secondPoint + SEGMENTS_SEPARATOR) == false
                    )
                {
                    segmentsAsString = segmentsAsString + firstPoint + SEGMENTS_START_END_SEPARATOR + secondPoint + SEGMENTS_SEPARATOR;
                }
            }

            //Remove last separator
            segmentsAsString = segmentsAsString.Remove(segmentsAsString.Length - SEGMENTS_SEPARATOR.Length);

            segmentsAsString = segmentsAsString + END_OF_BLOCK;

            return segmentsAsString;
        }

        /// <summary>
        /// Serialize points and segments.
        /// </summary>
        /// <param name="data">Data for serialization.</param>
        /// <returns>Serialized points and frames as string.</returns>
        static private string SerializeFrames(SerializableData data)
        {
            string framesAsString = FRAMES_HEADER;
            string pointX;
            string pointY;

            for (int i = 0; i < data.Frames.Length; i++)
            {
                framesAsString = framesAsString + FRAME_START;

                for (int j = 0; j < data.Frames[i].Positions.Count; j++)
                {
                    pointX = data.Frames[i].Positions[j].x.ToString();
                    pointY = data.Frames[i].Positions[j].y.ToString();

                    framesAsString = framesAsString + pointX + XY_SEPARATOR + pointY;

                    if (j < data.Frames[i].Positions.Count - 1)
                    {
                        framesAsString = framesAsString + POINTS_SEPARATOR;
                    }
                }

                framesAsString = framesAsString + FRAME_END;
            }

            framesAsString = framesAsString + END_OF_BLOCK;

            return framesAsString;
        }

        #endregion

        #region Deserialization

        /// <summary>
        /// Deserialize and load data from string
        /// </summary>
        /// <param name="text">string for deserialization</param>
        /// <returns>deserialized data</returns>
        static public SerializableData LoadFromString(string text)
        {
            SerializableData data = new SerializableData();

            int indexOfFrameHeader = text.IndexOf(FRAMES_HEADER);

            // extract blocks
            string segmentsBlock = text.Remove(indexOfFrameHeader, text.Length - indexOfFrameHeader);
            string framesBlock = text.Remove(0, indexOfFrameHeader);

            //Parse frames block
            Desk.Find().Frames = ParseFrames(framesBlock);

            //Fill Desk with points
            for (int i = 0; i < Desk.Find().Frames[0].Positions.Count; i++)
            {
                Desk.Find().Points.Add(Point.Create(new Vector3(Desk.Find().Frames[0].Positions[i].x, Desk.Find().Frames[0].Positions[i].y, (float)Layer.PointLayer)));
            }

            //Prepare segments block: 
            segmentsBlock = segmentsBlock.Remove(0, SEGMENTS_HEADER.Length); //remove header
            segmentsBlock = segmentsBlock.Remove(segmentsBlock.Length - END_OF_BLOCK.Length); //remove end_of_block

            //Parse segments block and create segments
            data.Segments = CreateSegments(segmentsBlock);

            return data;
        }

        /// <summary>
        /// Parses segments section and creates segments on desk, according to parsed data.
        /// </summary>
        /// <param name="segmentsBlock">Segments section for parse.</param>
        /// <returns>Array of created segments.</returns>
        private static Segment[] CreateSegments(string segmentsBlock)
        {
            //Split block to pieces:
            string[] segmentsPieces = segmentsBlock.Split(SEGMENTS_SEPARATOR.ToCharArray());

            //prepare container for segments
            Segment[] segments = new Segment[segmentsPieces.Length];

            Desk desk = Desk.Find();

            for (int i = 0; i < segmentsPieces.Length; i++)
            {
                if (segmentsPieces[i].Length > 2) //ignore empty pieces
                {
                    //Prepare segment`s data:
                    string startPointString = segmentsPieces[i].Remove(segmentsPieces[i].IndexOf(SEGMENTS_START_END_SEPARATOR));
                    string endPointString = segmentsPieces[i].Remove(0, segmentsPieces[i].IndexOf(SEGMENTS_START_END_SEPARATOR) + SEGMENTS_START_END_SEPARATOR.Length);
                    int startPointIndex = int.Parse(startPointString);
                    int endPointIndex = int.Parse(endPointString);

                    //Create segment on a desk:
                    segments[i] = Segment.Create(desk.Points[startPointIndex], desk.Points[endPointIndex]);
                }
            }

            return segments;
        }

        /// <summary>
        /// Parses frames, and builds List of Frames, containing points.
        /// </summary>
        /// <param name="framesBlock">Frames section to parse.</param>
        /// <returns>List of parsed frames.</returns>
        private static List<Frame> ParseFrames(string framesBlock)
        {
            //prepare block:
            framesBlock = framesBlock.Remove(0, FRAMES_HEADER.Length);
            framesBlock.Remove(0, FRAME_START.Length);
            framesBlock.Remove(framesBlock.Length - END_OF_BLOCK.Length - FRAME_END.Length, END_OF_BLOCK.Length + FRAME_END.Length);

            string framesDivider = FRAME_END + FRAME_START;

            //Split block to pieces:
            string[] framesPieces = framesBlock.Split(framesDivider.ToCharArray()); //Split frames section to pieces (separate frames)

            List<Frame> result = new List<Frame>(); //prepare container

            for (int i = 0; i < framesPieces.Length; i++)
            {
                if (framesPieces[i].Length > 2) //ignore empty pieces
                {
                    Frame parsedFrame = new Frame();
                    parsedFrame.Positions = new List<Vector2>();

                    string[] pointsPieces = framesPieces[i].Split(POINTS_SEPARATOR.ToCharArray());  //Split current frame string to pieces (separate points)

                    for (int j = 0; j < pointsPieces.Length; j++)
                    {
                        if (pointsPieces[j].Length > 2) //min 3 pieces needed to parse
                        {
                            int indexOfDivider = pointsPieces[j].IndexOf(XY_SEPARATOR);

                            if (indexOfDivider >= 0)
                            {
                                string xString = pointsPieces[j].Remove(indexOfDivider);
                                string yString = pointsPieces[j].Remove(0, indexOfDivider + XY_SEPARATOR.Length);
                                float xPos = float.Parse(xString);
                                float yPos = float.Parse(yString);

                                parsedFrame.Positions.Add(new Vector2(xPos, yPos));     //Add point position to frame
                            }
                        }
                    }
                    result.Add(parsedFrame);    
                }
            }
            return result;
        }

        #endregion
    }
}
