﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;

using AnimationEditor.FileBrowser;

namespace AnimationEditor
{
    #region Delegates

    public delegate void OnLoadFrameHandler();  //used to update segments positions
    public delegate void OnNewFileLoadedHandler();  //used to clean up old points and segments

    #endregion

    /// <summary>
    /// Represents working surface - manages points and frames.
    /// </summary>
    public class Desk : MonoBehaviour
    {
        #region Events

        public event OnLoadFrameHandler OnLoadFrame;
        public event OnNewFileLoadedHandler OnNewFileLoaded;

        #endregion

        #region PrivateMembers

        private const string EXTENSION = ".sva";

        private static Desk m_instance; //quick access from everywhere

        //This is not the <Infinity>, as in task, but more than 60 hours of 30 fps video for 10 points (theoretically), and I think it`s enough.
        //If not, capacity can be extended by spliting timeline to separate pieces, which can be stored on (and loaded from) HDD separately.
        private List<Frame> m_frames = new List<Frame>();   

        private int m_currentFrame = 0; //current frame number

        [SerializeField]
        private Text m_frameLabel;    //label to show frame number

        private List<Point> m_points = new List<Point>();   //All the points on a desk

        private Point m_previewPoint = null;    // Used as a start point to preview potential new segment. If null, then no preview at the moment.

        private LineRenderer m_segmentPreviewRenderer;  //Line renderer to preview potential new segment. 

        private bool m_interactive = true;  //Used for restrict interactions during save/load operations.

        #endregion

        #region PublicProperties

        public Point PreviewPoint
        {
            get
            {
                return m_previewPoint;
            }

            set
            {
                m_previewPoint = value;
            }
        }

        public List<Point> Points
        {
            get
            {
                return m_points;
            }

            set
            {
                m_points = value;
            }
        }

        public List<Frame> Frames
        {
            get
            {
                return m_frames;
            }

            set
            {
                m_frames = value;
            }
        }

        public bool Interactive
        {
            get
            {
                return m_interactive;
            }

            set
            {
                m_interactive = value;
            }
        }

        #endregion

        #region MonoBehaviourMessages

        private void Awake()
        {
            m_instance = this;  //Set up quick access
            m_segmentPreviewRenderer = gameObject.GetComponent<LineRenderer>(); //Set up preview renderer
        }

        private void Start()
        {
            Frames = new List<Frame>();
            Frames.Add(new Frame(m_points));
            m_frameLabel.text = m_currentFrame.ToString();
        }

        private void Update()
        {
            UpdateSegmentPreview();
            CheckInput();
        }

        #endregion

        /// <summary>
        /// Quick access
        /// </summary>
        /// <returns></returns>
        public static Desk Find()
        {
            return m_instance;
        }

        #region Input

        /// <summary>
        /// Check all inputs (except points drag and click - points are dragable and clickable)
        /// </summary>
        private void CheckInput()
        {
            if (Interactive)
            {
                // Create points and segments
                CheckLeftMouseButton();

                // Remove segments
                CheckRightMouseButton();

                // Switch frames
                CheckKeyboard();
            }
        }

        /// <summary>
        /// Switch frames
        /// </summary>
        private void CheckKeyboard()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                if (m_currentFrame > 0)
                {
                    CycleFrames(-1);
                }
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                CycleFrames(1);
            }

        }

        /// <summary>
        /// Remove segments
        /// </summary>
        private void CheckRightMouseButton()
        {
            if (Input.GetMouseButtonDown(1))
            {
                //Get mouse world position
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                worldPosition.z = (float)Layer.SegmentLayer;    //set layer

                Segment segmentAtPoint = GetSegmentAtPosition(worldPosition);
                if (segmentAtPoint != null)
                {
                    segmentAtPoint.Remove();
                }
            }
        }

        /// <summary>
        /// Create points and segments
        /// </summary>
        private void CheckLeftMouseButton()
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Get mouse world position
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                //If there is no points under pointer
                //if (!Physics2D.Raycast(new Vector2(worldPosition.x, worldPosition.y), Vector2.zero))
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    //If previewing new segment
                    if (PreviewPoint != null)
                    {
                        StopPreview(null);
                    }
                    else
                    {
                        //Create new point:

                        worldPosition.z = (float)Layer.PointLayer;  //set layer
                        Point newPoint = Point.Create(worldPosition);
                        Points.Add(newPoint);

                        //See comment about optimization inside method RemovePoint().
                        //Adding new point to all frames
                        for (int i = 0; i < Frames.Count; i++)
                        {
                            Frames[i].Positions.Add(newPoint.transform.position);
                        }
                    }

                }

            }
        }

        #endregion

        #region Frames

        /// <summary>
        /// Load frame
        /// </summary>
        /// <param name="frame">Frame to load</param>
        private void LoadFrame(Frame frame)
        {
            //Set points positions
            for (int i = 0; i < m_points.Count; i++)
            {
                m_points[i].transform.position = new Vector3 (frame.Positions[i].x, frame.Positions[i].y, (float) Layer.PointLayer);
            }

            //Update segments
            if (OnLoadFrame != null)
            {
                OnLoadFrame();
            }
        }

        /// <summary>
        /// Cycle frames forward or backward
        /// </summary>
        /// <param name="count">Number of frames to cycle</param>
        private void CycleFrames(int count)
        {
            //cycle
            m_currentFrame += count;

            //Create new frames, if out of bounds
            if (m_currentFrame >= Frames.Count)
            {
                //Add frames:
                for (int i = 0; i < count; i++)
                {
                    Frame newFrame = new Frame();
                    newFrame.Positions = new List<Vector2>(Frames[m_currentFrame - count].Positions);
                    Frames.Add(newFrame);
                }
            }

            //Load cycled frame
            LoadFrame(Frames[m_currentFrame]);

            //Update UI frame number
            m_frameLabel.text = m_currentFrame.ToString();

        }

        #endregion

        #region Points

        /// <summary>
        /// Set point`s position in current frame
        /// </summary>
        /// <param name="point">point to save</param>
        public void SavePointPosition(Point point)
        {
            Frames[m_currentFrame].Positions[m_points.IndexOf(point)] = new Vector2(point.transform.position.x, point.transform.position.y);
        }

        /// <summary>
        /// Safely removes point - from all frames, and also removes all segments, connected to given point.
        /// </summary>
        /// <param name="point">Point to remove</param>
        public void RemovePoint(Point point)
        {
            //remove all connected segments
            for (int i = 0; i < m_points.Count; i++)
            {
                if (m_points[i] != point)
                {
                    Segment connectionSegment = Point.IsPointsConnected(m_points[i], point);
                    if (connectionSegment != null)
                    {
                        connectionSegment.Remove();
                    }
                }
            }

            // Next can be optimized later by assigning to every point unique index (and store it in frames with assigned position - e.g. using Dictionary instead of List), instead of conformity m_points indexes to frame.positions indexes. 
            // Or by saving deleted and created point`s indexes, and process them on loadin another frame. Or even by storing in frame whole points, not just positions, and completely reload points on frame change.
            // But I did not see necessarity to do this in current task, because I don`t know, what length of animation will be typical.

            //Remove point`s position from all frames
            for (int j = 0; j < Frames.Count; j++)
            {
                Frames[j].Positions.RemoveAt(m_points.IndexOf(point));
            }

            m_points.Remove(point);

            //destroy point and children segments
            Destroy(point.gameObject);
        }

        #endregion
        
        #region Segments

        /// <summary>
        /// Is there any Segment at given position?
        /// </summary>
        /// <param name="position">position to check</param>
        /// <returns>returns segment at position. If no such one, returns null.</returns>
        private Segment GetSegmentAtPosition(Vector3 position)
        {
            //Try find segment at point
            for (int i = 0; i < Points.Count; i++)  //iterate all points
            {
                for (int j = 0; j < Points[i].Segments.Count; j++)  //iterate all segments in point
                {
                    if (Points[i].Segments[j].IsPointOnSegment(position))   //check, is position inside current segment
                    {
                        return Points[i].Segments[j];   //return current segment
                    }
                }
            }
            return null;    //Not founded
        }

        /// <summary>
        /// Initialize segment`s preview
        /// </summary>
        /// <param name="startPoint">Start point for potential segment.</param>
        public void StartPreview(Point startPoint)
        {
            m_segmentPreviewRenderer.SetVertexCount(2);
            m_segmentPreviewRenderer.SetPosition(0, startPoint.transform.position);
            m_segmentPreviewRenderer.SetPosition(1, GetWorldLayerPosition(Input.mousePosition, Layer.PreviewLayer));
            PreviewPoint = startPoint;
        }

        /// <summary>
        /// Update preview`s position
        /// </summary>
        private void UpdateSegmentPreview()
        {
            if (PreviewPoint != null)
            {
                m_segmentPreviewRenderer.SetPosition(1, GetWorldLayerPosition(Input.mousePosition, Layer.PreviewLayer));
            } 
        }

        /// <summary>
        /// End of preview - turn preview`s renderer off, and create new segment, if needed.
        /// </summary>
        /// <param name="endPoint">User`s selected point</param>
        public void StopPreview(Point endPoint)
        {
            //if any point pointed, and it`s not a start point
            if (endPoint != null && endPoint != PreviewPoint)
            {
                //if two points not connected already
                if (Point.IsPointsConnected(PreviewPoint, endPoint) == null)
                {
                    Segment.Create(PreviewPoint, endPoint);
                }
            }

            //turn off preview
            m_segmentPreviewRenderer.SetVertexCount(0);
            PreviewPoint = null;
        }

        #endregion

        #region Saving

        /// <summary>
        /// Listener for Save button. Shows file browser window.
        /// </summary>
        public void SaveListener()
        {
            Interactive = false;
            FileBrowserCallback callback = new FileBrowserCallback(SaveToFile);
            FileBrowserWindow.Show(GameObject.FindObjectOfType<Canvas>(), callback, EXTENSION);
        }

        
        /// <summary>
        /// Callback for file browser for saving file
        /// </summary>
        /// <param name="path">Path, defined by user in file browser. If user canceled procedure, empty string will be here.</param>
        private void SaveToFile(string path)
        {
            if (path.Length > 0)    //not cancelled
            {
                //Gather data:
                SerializableData dataForSave = GetDataForSave();

                //To string:
                string dataString = SerializableData.Serialize(dataForSave);

                //Save to HDD:
                File.WriteAllText(path, dataString);
            }

            Interactive = true;
        }

        /// <summary>
        /// Gathers data for serialization.
        /// </summary>
        /// <returns>Serializable data</returns>
        public SerializableData GetDataForSave()
        {
            //Prepare container
            SerializableData data = new SerializableData();
            data.Frames = new Frame[Frames.Count];
            data.Segments = new Segment[0];
            Frames.CopyTo(data.Frames);

            //Gathering segments data across all points
            Segment[] previousSegmentsArray;
            Segment[] currentSegmentsArray;
            for (int i = 0; i < Points.Count; i++)  //iterate all points
            {
                //initialize
                currentSegmentsArray = new Segment[Points[i].Segments.Count];
                previousSegmentsArray = new Segment[data.Segments.Length];

                //copy
                data.Segments.CopyTo(previousSegmentsArray, 0);
                Points[i].Segments.CopyTo(currentSegmentsArray);

                //Combine
                data.Segments = new Segment[currentSegmentsArray.Length + previousSegmentsArray.Length];
                previousSegmentsArray.CopyTo(data.Segments, 0);
                currentSegmentsArray.CopyTo(data.Segments, previousSegmentsArray.Length);
            }

            return data;
        }

        #endregion

        #region Loading

        /// <summary>
        /// Listener for Load button. Shows file browser window.
        /// </summary>
        public void LoadListener()
        {
            Interactive = false;
            FileBrowserCallback callback = new FileBrowserCallback(LoadFromFile);
            FileBrowserWindow.Show(GameObject.FindObjectOfType<Canvas>(), callback, EXTENSION);
        }

        /// <summary>
        /// Callback for file browser for loading file
        /// </summary>
        /// <param name="path">Path, defined by user in file browser. If user canceled procedure, empty string will be here.</param>
        private void LoadFromFile(string path)
        {
            if (path.Length > 0)    //not cancelled
            {
                if (OnNewFileLoaded != null)
                {
                    OnNewFileLoaded();
                }
                m_frames = new List<Frame>();
                m_points = new List<Point>();
                m_currentFrame = 0;
                m_frameLabel.text = m_currentFrame.ToString();
                SerializableData.LoadFromString(File.ReadAllText(path));
                LoadFrame(Frames[0]);
            }
            Interactive = true;
        }

        #endregion

        #region Utility

        /// <summary>
        /// Transforms screen position into world position with respect to given layer
        /// </summary>
        /// <param name="screenPosition">Screen position</param>
        /// <param name="layer">Layer</param>
        /// <returns>Layered world position</returns>
        static public Vector3 GetWorldLayerPosition(Vector3 screenPosition, Layer layer)
        {
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
            worldPosition.z = (int)layer;
            return worldPosition;
        }

        #endregion
    }
}

