﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace AnimationEditor.FileBrowser
{
    /// <summary>
    /// Delegate for performing callbacks
    /// </summary>
    /// <param name="filePath">Result file path. If <Cancel> button pressed, empty string will be returned.</param>
    public delegate void FileBrowserCallback(string filePath);


    /// <summary>
    /// File browser.
    /// Call using static Show() method.
    /// Callback will be performed to delegate argument.
    /// Does not allows HDD modifying.
    /// TODO: implement HDD selection.
    /// TODO: implement folder creation.
    /// </summary>
    public class FileBrowserWindow : MonoBehaviour
    {

        #region PrivateMembers

        [SerializeField]
        private GameObject m_filePrefab;

        [SerializeField]
        private GameObject m_dirPrefab;

        [SerializeField]
        private Transform m_filesContainer;

        [SerializeField]
        private Transform m_dirContainer;

        [SerializeField]
        private InputField m_inputField;


        private string m_currentDirectory;
        private string m_currentFile;
        private string m_filter;

        private FileBrowserCallback m_browsingCallback;

        #endregion

        #region PublicProperties

        public FileBrowserCallback BrowsingCallback
        {
            get
            {
                return m_browsingCallback;
            }

            set
            {
                m_browsingCallback = value;
            }
        }

        public string Filter
        {
            get
            {
                return m_filter;
            }

            set
            {
                m_filter = value;
            }
        }

        #endregion

        /// <summary>
        /// Static caller
        /// </summary>
        /// <param name="canvas">Canvas to be child of</param>
        /// <param name="callback">Callback delegate</param>
        /// <param name="filter">filter files by extension</param>
        public static void Show(Canvas canvas, FileBrowserCallback callback, string filter = "")
        {
            GameObject fileBrowser = Instantiate(Resources.Load("Prefabs/FileBrowser"), Vector3.zero, Quaternion.identity) as GameObject;
            fileBrowser.transform.SetParent(canvas.transform);
            FileBrowserWindow fileBrowserBehaviour = fileBrowser.GetComponent<FileBrowserWindow>();
            fileBrowserBehaviour.BrowsingCallback = callback;
            fileBrowserBehaviour.Filter = filter;

            //Fix RectTransform position:
            RectTransform rTransform = fileBrowser.GetComponent<RectTransform>();
            rTransform.sizeDelta = Vector2.zero;
            rTransform.anchoredPosition = Vector2.zero;
        }

        void Start()
        {
            //Load initial directory
            LoadDirs(Directory.GetCurrentDirectory());
        }

        /// <summary>
        /// Loads list of directories from given path into directory view.
        /// Directories represented as buttons.
        /// </summary>
        /// <param name="currentDir">Path for get directories from.</param>
        private void LoadDirs(string currentDir)
        {
            //Clean up files and dirs views:
            ClearChilds(m_dirContainer);
            ClearChilds(m_filesContainer);

            m_currentDirectory = currentDir;

            string[] dirs = Directory.GetDirectories(currentDir);   //Get info

            for (int i = 0; i < dirs.Length; i++)
            {
                GameObject newDir = Instantiate(m_dirPrefab, Vector3.zero, Quaternion.identity) as GameObject;
                newDir.transform.SetParent(m_dirContainer);   //Put to container
                newDir.name = dirs[i];  //Save full path as gameObject`s name
                newDir.GetComponentInChildren<Text>().text = dirs[i].Remove(0, currentDir.Length);  //Crop path to represent directory name
                newDir.GetComponentInChildren<Button>().onClick.AddListener(() => DirectoryListener(newDir.name));  //Link button to listener
            }

            LoadFiles(currentDir);  //Load files from current dir
        }

        /// <summary>
        /// Loads list of files from given directory into files view.
        /// Files represented as buttons.
        /// </summary>
        /// <param name="currentDir">Path for get files from.</param>
        private void LoadFiles(string currentDir)
        {
            string[] files = Directory.GetFiles(currentDir);    //Get info

            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].EndsWith(Filter))  //filtering
                {
                    GameObject newFile = Instantiate(m_filePrefab, Vector3.zero, Quaternion.identity) as GameObject;
                    newFile.transform.SetParent(m_filesContainer);    //Put button to container
                    newFile.name = files[i];    //save full path as name
                    newFile.GetComponentInChildren<Text>().text = files[i].Remove(0, currentDir.Length);    //crop full path to represent only file name in button`s text field
                    newFile.GetComponentInChildren<Button>().onClick.AddListener(() => FileListener(newFile.name));     //Link button to listener
                }
            }
        }

        #region ControlsListeners

        /// <summary>
        /// Listener for back button - loads parent directory of selected directory.
        /// </summary>
        public void BackButtonListener()
        {
            LoadDirs(Directory.GetParent(m_currentDirectory).FullName);
        }

        /// <summary>
        /// Listener for cancel button. Performs empty callback and destroys window.
        /// </summary>
        public void CancelButtonListener()
        {
            BrowsingCallback("");
            Destroy(gameObject);
        }

        /// <summary>
        /// Listener for InputField - updates currentFile
        /// </summary>
        public void InputListener()
        {
            m_currentFile = m_inputField.text;
     
            if (m_currentFile.Contains(m_currentDirectory) == false)
            {
                m_currentFile = m_currentDirectory + Path.DirectorySeparatorChar + m_currentFile;
            }

            if (!m_currentFile.EndsWith(Filter))
            {
                m_currentFile = m_currentFile + Filter;
            }
        }

        /// <summary>
        /// Listener for button OK - performs callback and destroys window.
        /// </summary>
        public void OkButtonListener()
        {
            if (m_currentFile.EndsWith(Filter))
            {
                BrowsingCallback(m_currentFile);
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Listener for every directory button - loads given directory.
        /// </summary>
        /// <param name="selectedDirectoryPath">Path to selected directory.</param>
        public void DirectoryListener(string selectedDirectoryPath)
        {
            LoadDirs(selectedDirectoryPath);
        }

        /// <summary>
        /// Listener for every file button - updates currentFile and InputField text.
        /// </summary>
        /// <param name="selectedFileName">selected file name</param>
        public void FileListener(string selectedFileName)
        {
            m_currentFile = selectedFileName;
            m_inputField.text = m_currentFile;
        }

        #endregion

        #region Utility

        /// <summary>
        /// Destroy all childs of given transform.
        /// </summary>
        /// <param name="transform">transform to destroy childs of.</param>
        private void ClearChilds(Transform transform)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        #endregion

    }

}