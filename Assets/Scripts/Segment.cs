﻿using UnityEngine;

namespace AnimationEditor
{
    /// <summary>
    /// Represents Segment, connecting two points.
    /// </summary>
    public class Segment : MonoBehaviour
    {
        #region PrivateMembers

        private Point m_startPoint;
        private Point m_endPoint;

        private LineRenderer m_lineRenderer;

        #endregion

        #region PublicProperties

        public Point StartPoint
        {
            get
            {
                return m_startPoint;
            }

            set
            {
                m_startPoint = value;
            }
        }

        public Point EndPoint
        {
            get
            {
                return m_endPoint;
            }

            set
            {
                m_endPoint = value;
            }
        }

        #endregion
        
        /// <summary>
        /// Static creator
        /// </summary>
        /// <param name="startPoint">Start point. Segment transform will be set as child of this point.</param>
        /// <param name="endPoint">End point.</param>
        /// <returns>Created Segment.</returns>
        public static Segment Create(Point startPoint, Point endPoint)
        {
            GameObject prefab = Resources.Load("Prefabs/Segment") as GameObject;
            GameObject newSegment = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
            newSegment.transform.SetParent(startPoint.transform);
            Segment segmentBehaviour = newSegment.GetComponent<Segment>();

            //Subscribe event
            Desk.Find().OnLoadFrame += segmentBehaviour.HandleOnLoadFrame;
            Desk.Find().OnNewFileLoaded += segmentBehaviour.HandleOnNewFileLoaded;

            //assign segment to points and vice versa
            segmentBehaviour.StartPoint = startPoint;
            segmentBehaviour.EndPoint = endPoint;
            startPoint.Segments.Add(segmentBehaviour);
            endPoint.Segments.Add(segmentBehaviour);

            segmentBehaviour.Initialize();

            return segmentBehaviour;
        }

        /// <summary>
        /// Unsubscribe from events on destroy.
        /// </summary>
        private void OnDestroy()
        {
            Desk.Find().OnLoadFrame -= HandleOnLoadFrame;
            Desk.Find().OnNewFileLoaded -= HandleOnNewFileLoaded;
        }

        /// <summary>
        /// Initialize LineRenderer.
        /// </summary>
        private void Initialize()
        {
            m_lineRenderer = gameObject.GetComponent<LineRenderer>();
            m_lineRenderer.SetVertexCount(2);
            UpdatePosition();
        }

        /// <summary>
        /// Removes Segment safely.
        /// </summary>
        public void Remove()
        {
            m_startPoint.Segments.Remove(this);
            m_endPoint.Segments.Remove(this);
            Destroy(gameObject);
        }

        /// <summary>
        /// Update segment`s position
        /// </summary>
        public void UpdatePosition()
        {
            m_lineRenderer.SetPosition(0, StartPoint.transform.position);
            m_lineRenderer.SetPosition(1, EndPoint.transform.position);
        }

        #region EventHandlers

        /// <summary>
        /// Update position when frame changed
        /// </summary>
        private void HandleOnLoadFrame()
        {
            UpdatePosition();
        }

        /// <summary>
        /// Destroy old segments when new file loaded
        /// </summary>
        private void HandleOnNewFileLoaded()
        {
            Destroy(gameObject);
        }

        #endregion

        /// <summary>
        /// Is given position is on segment?
        /// </summary>
        /// <param name="position">position for check</param>
        /// <returns>true, if it is.</returns>
        public bool IsPointOnSegment(Vector3 position)
        {
            Vector3 segmentStartOnLayer = m_startPoint.transform.position;
            Vector3 segmentEndOnLayer = m_endPoint.transform.position;

            //move coordinates of start and end points to segment`s layer:
            segmentStartOnLayer.z = (float) Layer.SegmentLayer;
            segmentEndOnLayer.z = (float)Layer.SegmentLayer;

            //Get sides of triangle, formed by segment and point
            Vector3 segmentVector = segmentEndOnLayer - segmentStartOnLayer;
            Vector3 pointVector1 = position - segmentStartOnLayer;
            Vector3 pointVector2 = segmentEndOnLayer - position;

            //Get length of two sides of triangle, which is not a segment:
            float lineLength = pointVector1.magnitude + pointVector2.magnitude;

            //Get dostance from point to nearest segment`s (or it`s infinite line) point:
            Vector3 projection = Vector3.Project(pointVector1, segmentVector);
            float distanceToLine = Vector3.Distance(pointVector1, projection);

            if (lineLength - segmentVector.magnitude < 0.1f  && distanceToLine < 0.1f)
            {
                return true;
            }
            return false;
        }
    }
}
