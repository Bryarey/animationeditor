﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace AnimationEditor.FileBrowser
{

    /// <summary>
    /// Preforms "Fit to width" operation for grid layout. Used in file browser.
    /// </summary>
    public class FitGrid : MonoBehaviour
    {
        void Start()
        {
            GridLayoutGroup gridLayout = gameObject.GetComponent<GridLayoutGroup>();
            RectTransform rTransform = transform.parent.gameObject.GetComponent<RectTransform>();

            Vector2 newCellSize = gridLayout.cellSize;
            newCellSize.x = rTransform.rect.width / gridLayout.constraintCount;
            gridLayout.cellSize = newCellSize;

        }
    }
}
