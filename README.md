# README #

This is a simple 
# Animation editor #

There is a three entities to dealing with: points, segments and frames.

### Point
can be added, dragged, and removed.

### Segment 
can be added and removed.

### Frames
can be cycled back and forward from 0 to ... 
When you cycle frame forward, it automatically added.
Points positions are saved every frame.
Current frame number displayed at top-left corner.

## Controls: ##
- **left click** on empty space to **create point**
- **left click** on exist point to start **creating segment**, then **left-click** on another point to **create** (or click on empty space to cancel)
- **left click** and drag point to change it`s **position**
- **right click** on point or segment to **remove** it
- Press **D** key to cycle frames **forward**
- Press **A** key to cycle frames **backwards**

You can **save or load** animations to HDD. Press **<Save>** or **<Load>** buttons to do this.

Simple** file browser** included to select files. It based on System.IO library. You can not switch drives at this moment (just because my current Unity3D version does not supports dropdowns).

Files stored in specific human-readable *.sva format.